package types

import(
	"container/list"
	"sync"
)

// ListWithLock 带锁的list. 要保证协程安全, 在使用时需要合理利用锁的机制.
// 当前协程安全的方法调用有: PushBack, Front, Remove,这些方法在与非协程安全的方法同时调用时,仍是不安全的.
type ListWithLock struct{
	sync.RWMutex
	*list.List
}


func NewListWithLock()*ListWithLock{
	return &ListWithLock{
		List: list.New(),
	}
}

func(lwl *ListWithLock)PushBack(v interface{})*list.Element{
	lwl.Lock()
	elm := lwl.List.PushBack(v)
	lwl.Unlock()
	return elm
}

func(lwl *ListWithLock)Front()*list.Element{
	lwl.RLock()
	elem := lwl.List.Front()
	lwl.RUnlock()
	return elem
}

func(lwl *ListWithLock)Remove(e *list.Element)interface{}{
	lwl.Lock()
	v := lwl.List.Remove(e)
	lwl.Unlock()
	return v
}